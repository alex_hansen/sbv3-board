/*********************************************************************
 * SMARTBOARDV3
 *
 * This is the code for the SmartBoardV3 board/grid unit.
 *
 * Major Updates:
 * 2021-06-04
 * - Added DEBUG flag
 * 2021-05-16
 * - Updated to State Machine.
 * - Added Test Funciton for LEDs
 *
 * Author - Alex Hansen
 * Date   - 2021-05-16
 *********************************************************************/

/*********************************************************************
 * Settings
 *********************************************************************/

#define BAUD 115200
#define VERSION 20210604
//#define DEBUG

/*********************************************************************
 * Device Role
 *********************************************************************/

#define ROLE_PIECE   0
#define ROLE_GRID    1
#define ROLE_CONTROL 2

#define ROLE ROLE_GRID

/*********************************************************************
 * Add wifi include to get MAC Address
 *********************************************************************/
#ifdef ESP32
  #include <WiFi.h>
#else
  #include <ESP8266WiFi.h>
#endif

/*********************************************************************
 * PAINLESS MESH
 *********************************************************************/

#include <painlessMesh.h>

#define   MESH_SSID       "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

const uint32_t CONTROL_UNIT_ADDR = 469929400;
painlessMesh  mesh;

// Painless Mesh Prototypes
void receivedCallback(uint32_t from, String & msg);
void newConnectionCallback(uint32_t nodeId);
void changedConnectionCallback(); 
void nodeTimeAdjustedCallback(int32_t offset); 
void delayReceivedCallback(uint32_t from, int32_t delay);

/*********************************************************************
 * Scheduler
 *********************************************************************/

Scheduler     userScheduler; // to control our task.

/*********************************************************************
 * State Machines
 *********************************************************************/
byte current_state = 0;
byte new_state = 0;

// Prototype the switch state function.
void switch_state( int new_state );

//Define the states, and any variables that the state uses:
#define STATE_STARTUP 1
byte state_test_led = 0;
byte state_test_led_color = 1; // This is COLOR_RED

#define STATE_RUNNING 2

/*********************************************************************
 * WS2812B LEDs
 *********************************************************************/

#define LED_PIN D1  // Pin of the WS2812bs
#define LED_NUM 16 // We have 16 LEDs connected

#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel leds = Adafruit_NeoPixel(LED_NUM, LED_PIN, NEO_GRB);

//Simple array to convert from Pin locations to LED numbers:
byte pin_to_led_map[] = {6,7,4,5,2,3,0,1,8,9,10,11,12,13,14,15};

//Quick notes on layout:
/* LEDs start at the bottom right segment and move in a sirpentine pattern:
+--+--++--+--+
| 9| 8|| 7| 6|
+--+--++--+--+
|10|11|| 4| 5|
+--+--++--+--+
+--+--++--+--+
|13|12|| 3| 2|
+--+--++--+--+
|14|15|| 0| 1|
+--+--++--+--+

//Pin Locations:
+--+--++--+--+
| 9| 8|| 1| 0|
+--+--++--+--+
|10|11|| 2| 3|
+--+--++--+--+
+--+--++--+--+
|13|12|| 5| 4|
+--+--++--+--+
|14|15|| 6| 7|
+--+--++--+--+
*/

/*********************************************************************
 * LED Colors
 *********************************************************************/

#define COLOR_BLACK 0
#define COLOR_RED   1
#define COLOR_BLUE  2
#define COLOR_GREEN 3
#define COLOR_WHITE 4
#define COLOR_MAX   5

struct RGB {
  byte red;
  byte green;
  byte blue;
} colors[ COLOR_MAX ] = {
  {   0,   0,   0 },  // COLOR_BLACK
  { 255,   0,   0 },  // COLOR_RED
  {   0,   0, 255 },  // COLOR_BLUE
  {   0, 225,   0 },  // COLOR_GREEN
  { 255, 255, 255 }   // COLOR_WHITE
};

/*********************************************************************
 * 74HC165 Setup
 *********************************************************************/

#include <SPI.h>

#define LATCH D0
const byte chips = 2; // number of 74HC165 (8bit) chips used
byte results_array[chips]; //An array to store our incoming results.

/*********************************************************************
 * GRID ARRAY
 *********************************************************************/

// Define an array to store our grid contencts
#define NUMBER_OF_SQUARES 16
boolean grid_squares[ NUMBER_OF_SQUARES ];

/*********************************************************************
 * Helpers
 *********************************************************************/
void send_message( String message ) {

#ifdef DEBUG
  Serial.print("Sending message \""); Serial.print( message ); Serial.println("\"");
#endif

  mesh.sendBroadcast( message);  
  //mesh.sendSingle( CONTROL_UNIT_ADDR, message);
}

void fill_grid_squares( boolean fill_value ) {
  for (byte i=0; i<NUMBER_OF_SQUARES; i++)
    set_grid_sqaure( i, fill_value );
}
void print_grid_squares( void ) {
  for (byte i=0; i<NUMBER_OF_SQUARES; i++)
    Serial.print((grid_squares[i]==true ) ? '1' : '0');
}
void print_raw_grid_squares( void ) {
  for (byte i=0; i<NUMBER_OF_SQUARES; i++) {
    if (grid_squares[i] == true )
      Serial.print('T');
    else
      Serial.print("_");
  }
}
void set_grid_sqaure( byte index, boolean value ) {
  if ( grid_squares[ index ] != value ) {
    if ( value == true ) {
      String message = "{ Insert : \"";
      message += String( index );
      message += "\"}";
      send_message( message );
    }
    else { // Value == false
      String message = "{ Removal : \"";
      message += String( index );
      message += "\"}";
      send_message( message );    
    }
  }
  grid_squares[index] = value;
}
void set_grid_squares_from_byte( byte values, byte offset ) {
  for (byte i=0; i<8; i++) {
    set_grid_sqaure( i+offset, bitRead( values, i) );
  }
}

void set_led_color( byte led, byte color_value ) {
  //Set the color of a pixel.
  leds.setPixelColor( led, leds.Color( colors[color_value].red, colors[color_value].green, colors[color_value].blue ) );
}
void process_leds() {
  // Sets the colors of the LEDs based on the grid_sqaures array. This may need a map function for the layout of the board.
  for( int i = 0; i < LED_NUM; i++ ){
    if ( grid_squares[ i ] == true )
      set_led_color( pin_to_led_map[ i ], COLOR_BLUE );
    else
      set_led_color( pin_to_led_map[ i ], COLOR_BLACK );
  }

  leds.show();
}

String get_role() {
  switch (ROLE) {
    case ROLE_PIECE : return String("Piece"); break;
    case ROLE_GRID  : return String("Grid"); break;
    case ROLE_CONTROL : return String("Control"); break;
  }
  return String("Error");
}
/*********************************************************************
 * User Tasks
 *********************************************************************/

void test_led_Function( void ) {
  /* Quick test funciton to test the LEDs.
  This will cycle through red->green->blue->black. */

  if ( state_test_led < LED_NUM ) {
    //Change the color of the current led and increment it.
#ifdef DEBUG    
    Serial.print("Testing LED "); Serial.print( state_test_led ); Serial.print(" using color "); Serial.println( state_test_led_color );
#endif

    set_led_color( state_test_led, state_test_led_color );
    state_test_led++;
  }
  else {
    //Check to see if we should move on to a new color. If not, switch into the running state.
    state_test_led = 0;
    switch ( state_test_led_color ) {
      case COLOR_RED   : state_test_led_color = COLOR_GREEN; break;
      case COLOR_GREEN : state_test_led_color =  COLOR_BLUE; break;
      case COLOR_BLUE  : state_test_led_color = COLOR_BLACK; break;
      case COLOR_BLACK : switch_state( STATE_RUNNING ); break;
    }
  };

  //We have to manually call this here. Although maybe this should be dumped into the loop? Or its own task?
  leds.show();  
}
Task test_led_FunctionTask( TASK_SECOND * 0.1, TASK_FOREVER, &test_led_Function );

void userFunction( void ) {
  // Simple function that reads in the grid data and updates the LEDs as needed.
  digitalWrite (LATCH, LOW); 
  digitalWrite (LATCH, HIGH); 

  for (byte i=0; i<chips; i++) 
    results_array[i] = SPI.transfer(0);

  //fill_grid_squares( false );
  set_grid_squares_from_byte( results_array[0], 0 );
  set_grid_squares_from_byte( results_array[1], 8 );

#ifdef DEBUG
  print_grid_squares(); Serial.print(" "); print_raw_grid_squares(); Serial.print(" "); Serial.println();
#endif

  process_leds();
}
Task userFunctionTask( TASK_SECOND * 0.1, TASK_FOREVER, &userFunction );

void setup_tasks( void ) {
  //Load all the tasks into the user Scheduler. This might need to be updated when the states switch, but ...
  userScheduler.addTask( userFunctionTask );  
  userScheduler.addTask( test_led_FunctionTask );
}

/*********************************************************************
 * Setup
 *********************************************************************/
void mesh_setup() {
  // MESH setup
  mesh.setDebugMsgTypes(ERROR | DEBUG);  // set before init() so that you can see error messages

  mesh.init(MESH_SSID, MESH_PASSWORD, &userScheduler, MESH_PORT);

  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
  mesh.onNodeDelayReceived(&delayReceivedCallback);
}

void setup() {
  // Serial setup
  Serial.begin( BAUD );

  //mesh setup
  mesh_setup();

  // SPI Setup
  SPI.begin (); 
  SPI.beginTransaction(SPISettings(32000000, MSBFIRST, SPI_MODE2)); // 32MHz
  pinMode (LATCH, OUTPUT); 
  digitalWrite (LATCH, HIGH);

  // Grid Setup
  fill_grid_squares( false );

  // LED Setup
  leds.begin();   

  // Tasks Setup
  setup_tasks();

  // Switch to the start up tasks
  switch_state( STATE_STARTUP );
}

/*********************************************************************
 * Loop
 *********************************************************************/
void loop() {
  mesh.update();
}

/*********************************************************************
 * STATE MACHINE
 *********************************************************************/
void switch_state( int new_state ) {
  /*
  Switch from one state to another. This will short circuit is the state doesn't really change.
  */

  //Check to see if we've changed states, and if not return
  if ( current_state == new_state ) return; 

#ifdef DEBUG
  Serial.print("Changing state from "); Serial.print( current_state ); Serial.print(" to "); Serial.println( new_state );
#endif

  //Stop all tasks, the state list below should enable them as needed.
  test_led_FunctionTask.disable();
  userFunctionTask.disable();  

  switch ( new_state ) {
    case STATE_STARTUP : { test_led_FunctionTask.enable(); break; }
    case STATE_RUNNING : { userFunctionTask.enable(); break; }
  }

  // Now update our current state to the new state
  current_state = new_state;  
}

/*********************************************************************
 * PAINLESS MESH CALL BACK FUNCTIONS DECLARED ABOVE
 *********************************************************************/
void receivedCallback(uint32_t from, String & msg) { }
void newConnectionCallback(uint32_t nodeId) { }
void changedConnectionCallback() { }
void nodeTimeAdjustedCallback(int32_t offset) { }
void delayReceivedCallback(uint32_t from, int32_t delay) { }